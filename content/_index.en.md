---
title: "Home"
date: 2018-10-13T19:26:34+02:00
draft: false
type: "home"
description: "Vikunja - the open-source to-do app to organize your life."
heading:
  heading: Vikunja (/vɪˈkuːnjə/)
  content: The to-do app to organize your life.
  subtitle: Also one of the two wild South American camelids which live in the high<br/>
            alpine areas of the Andes and a [relative of the llama](https://en.wikipedia.org/wiki/Vicu%C3%B1a).
  subsubtitle: Vikunja is an Open-Source, [self-hosted](https://vikunja.io/docs/installing/) To-Do list application for all platforms.
               It is licensed under the [AGPLv3](https://code.vikunja.io/api/src/branch/main/LICENSE). 
menu:
  page:
    title: "Home"
    weight: 10
---

<div class="container">
<a class="button is-primary is-large" href="https://try.vikunja.io" target="_blank" rel="noopener">Try it</a>
<a class="button is-large" href="/features">Features</a>
</div>
