---
title: "Contact"
date: 2018-11-19T22:07:56+02:00
draft: false
type: "page"
---

# Contact

We have public issue trackers in the different [code repos](https://code.vikunja.io).

#### Chat

You can chat with us on [matrix](https://riot.im/app/#/room/!dCRiCiLaCCFVNlDnYs:matrix.org?via=matrix.org).

#### Forum

A community forum is available on [community.vikunja.io](https://community.vikunja.io).

#### General

[hello@vikunja.io](mailto:hello@vikunja.io)

General inqueries and requests.

#### Maintainers

[maintainers@vikunja.io](mailto:maintainers@vikunja.io)

Say hello to our maintainers.

#### Security 

See [our dedicated Security page](https://vikunja.io/security).
