---
title: "Imprint"
date: 2019-02-06T16:46:30+02:00
draft: false
---

# Imprint

Angaben gemäß § 5 TMG

Konrad Langenberg
Feldbergstraße 19
35043 Marburg

## Contact

E-Mail: <a href="mailto:hello@vikunja.io">hello@vikunja.io</a>

## Online Dispute Resolution website of the EU Commission

In order for consumers and traders to resolve a dispute out-of-court, the European Commission developed the
Online Dispute Resolution Website: [ec.europa.eu/consumers/odr](https://www.ec.europa.eu/consumers/odr)

## Legal disclaimer

The contents of these pages were prepared with utmost care. Nonetheless, we cannot assume liability for the
timeless accuracy and completeness of the information.

Our website contains links to external websites. As the contents of these third-party websites are beyond our
control, we cannot accept liability for them. Responsibility for the contents of the linked pages is always held
by the provider or operator of the pages.

Source: <a href="https://language-boutique.de/muster-impressum" target="_blank" rel="noopener">Language-Boutique.de/Muster-Impressum</a>