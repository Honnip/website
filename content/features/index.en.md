---
title: "Features"
date: 2018-10-13T21:59:47+02:00
draft: false
type: "features"
menu:
  page:
    title: "Features"
    weight: 20
---

<section class="hero is-medium is-bold feature-hero">
  <div class="hero-body">
    <div class="container has-text-centered is-white">
      <h1 class="title">
        Think of Vikunja like the notebook you have with all your things to keep track of.
      </h1>
      <h2 class="subtitle">
        But better.
      </h2>
    </div>
  </div>
</section>

<div class="container is-white">
    <div class="content">
        <div class="columns feature">
            <div class="column theimage">
                <video autoplay loop>
                	<source src="tasks.webm" type='video/webm;codecs="vp8, vorbis"'/>
                	<img src="tasks-preview.png" alt=""/>
                </video>
            </div>
            <div class="column">
                <h2>Stay organized</h2>
                <p>Organize your tasks in lists, organize lists in namespaces.</p>
                <h2>Tasks</h2>
                <p>
                	Tasks are not only simple tasks. You can let Vikunja remind you of tasks when they're due. 
	                Never miss an important deadline again!
	            </p>
                <p>
                	Remember that thing you have to do every week but can't get the hang on? 
                	You can set tasks to <i>repeat in a time interval</i>, letting Vikunja remind you of important weekly or monthly tasks.
                </p>
                <p>Vikunja also lets you split a task in multiple subtasks for easy progress tracking and more satisfaction when crossing them off the list!</p>
            </div>
        </div>

<div class="columns feature">
    <div class="column">
        <h2>
        	<span class="icon is-small"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="users" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" class="svg-inline--fa fa-users fa-w-20"><path fill="currentColor" d="M96 224c35.3 0 64-28.7 64-64s-28.7-64-64-64-64 28.7-64 64 28.7 64 64 64zm448 0c35.3 0 64-28.7 64-64s-28.7-64-64-64-64 28.7-64 64 28.7 64 64 64zm32 32h-64c-17.6 0-33.5 7.1-45.1 18.6 40.3 22.1 68.9 62 75.1 109.4h66c17.7 0 32-14.3 32-32v-32c0-35.3-28.7-64-64-64zm-256 0c61.9 0 112-50.1 112-112S381.9 32 320 32 208 82.1 208 144s50.1 112 112 112zm76.8 32h-8.3c-20.8 10-43.9 16-68.5 16s-47.6-6-68.5-16h-8.3C179.6 288 128 339.6 128 403.2V432c0 26.5 21.5 48 48 48h288c26.5 0 48-21.5 48-48v-28.8c0-63.6-51.6-115.2-115.2-115.2zm-223.7-13.4C161.5 263.1 145.6 256 128 256H64c-35.3 0-64 28.7-64 64v32c0 17.7 14.3 32 32 32h65.9c6.3-47.4 34.9-87.3 75.2-109.4z" class=""></path></svg></span>
	        Collaboration
	    </h2>
        <p>
        Ever wished you could just share that grocery list with your roomate instead of having to send dozens of texts on your way to the supermarket?
           With Vikunja, you can. Simply share a list (or a namespace with all its lists) to another user.
           Don't want your roommate to add new things to the grocery list and only do the shopping? 
           You can also share a list with <i>read-only</i> access!
        </p>   
        <p>
         Planning a bigger thing? You can use teams to share a list or namespace with multiple people at a time!
        </p>
        <h3>
            <span class="icon is-small"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="share-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-share-alt fa-w-14 fa-3x"><path fill="currentColor" d="M352 320c-22.608 0-43.387 7.819-59.79 20.895l-102.486-64.054a96.551 96.551 0 0 0 0-41.683l102.486-64.054C308.613 184.181 329.392 192 352 192c53.019 0 96-42.981 96-96S405.019 0 352 0s-96 42.981-96 96c0 7.158.79 14.13 2.276 20.841L155.79 180.895C139.387 167.819 118.608 160 96 160c-53.019 0-96 42.981-96 96s42.981 96 96 96c22.608 0 43.387-7.819 59.79-20.895l102.486 64.054A96.301 96.301 0 0 0 256 416c0 53.019 42.981 96 96 96s96-42.981 96-96-42.981-96-96-96z" class=""></path></svg></span>
            Share links
        </h3>
        <p>
            You can share a list with a link so that others can directly see or edit all tasks on a list but don't need to create an account.
            Share links have all the same rights management as sharing with users or teams.
        </p>
    </div>
    <div class="column theimage">
        <img src="sharing-user.png" alt="Sharing with a user" style="margin-top:3em"/>
    </div>
</div>

<div class="columns feature">
    <div class="column theimage">
        <video autoplay loop>
        	<source src="kanban.webm" type='video/webm;codecs="vp8, vorbis"'/>
        	<img src="kanban.png" alt=""/>
        </video>
    </div>
    <div class="column">
        <h2>Kanban board</h2>
        <p>
            Vikunja lets you organize and prioritize your tasks in a kanban board.<br/>
            Quickly get an overview of the status of things - especially when collaborating with multiple people on a project.
        <p>
        <p>
            Each card on a board represents a task in Vikunja.<br/>
            All tasks can be added through the usual ways and will then show up in the kanban board where they can be easily rearranged and reordered.
        </p>
    </div>
</div>

<div class="columns feature">
    <div class="column">
        <h3>
	        <span class="icon is-small"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="tags" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512" class="svg-inline--fa fa-tags fa-w-20"><path fill="currentColor" d="M497.941 225.941L286.059 14.059A48 48 0 0 0 252.118 0H48C21.49 0 0 21.49 0 48v204.118a48 48 0 0 0 14.059 33.941l211.882 211.882c18.744 18.745 49.136 18.746 67.882 0l204.118-204.118c18.745-18.745 18.745-49.137 0-67.882zM112 160c-26.51 0-48-21.49-48-48s21.49-48 48-48 48 21.49 48 48-21.49 48-48 48zm513.941 133.823L421.823 497.941c-18.745 18.745-49.137 18.745-67.882 0l-.36-.36L527.64 323.522c16.999-16.999 26.36-39.6 26.36-63.64s-9.362-46.641-26.36-63.64L331.397 0h48.721a48 48 0 0 1 33.941 14.059l211.882 211.882c18.745 18.745 18.745 49.137 0 67.882z" class=""></path></svg></span>
        	Labels
       	</h3>
        <p>
            Effortlessly mark tasks with a colorful label to find and group relevant tasks with a click!
        </p>
        <h3>
	        <span class="icon is-small"><svg aria-hidden="true" focusable="false" data-prefix="far" data-icon="star" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" class="svg-inline--fa fa-star fa-w-18"><path fill="currentColor" d="M528.1 171.5L382 150.2 316.7 17.8c-11.7-23.6-45.6-23.9-57.4 0L194 150.2 47.9 171.5c-26.2 3.8-36.7 36.1-17.7 54.6l105.7 103-25 145.5c-4.5 26.3 23.2 46 46.4 33.7L288 439.6l130.7 68.7c23.2 12.2 50.9-7.4 46.4-33.7l-25-145.5 105.7-103c19-18.5 8.5-50.8-17.7-54.6zM388.6 312.3l23.7 138.4L288 385.4l-124.3 65.3 23.7-138.4-100.6-98 139-20.2 62.2-126 62.2 126 139 20.2-100.6 98z" class=""></path></svg></span>
        	Priorities
        </h3>
        <p>
        	Know that feeling when you have a dozen things to do but can't decide on what to work next? Vikunja lets you prioritize your tasks with a few 
			clicks, so you'll always know on what to work next.
		</p>
        <h3>
        	<span class="icon is-small"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="user" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-user fa-w-14 fa-3x"><path fill="currentColor" d="M224 256c70.7 0 128-57.3 128-128S294.7 0 224 0 96 57.3 96 128s57.3 128 128 128zm89.6 32h-16.7c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16h-16.7C60.2 288 0 348.2 0 422.4V464c0 26.5 21.5 48 48 48h352c26.5 0 48-21.5 48-48v-41.6c0-74.2-60.2-134.4-134.4-134.4z" class=""></path></svg></span>
	        Delegation
	    </h3>
        <p>Assign tasks to team members so everyone knows what to do.</p>
    </div>
    <div class="column theimage">
        <img src="tasks.png" alt="Sharing with a user" style="margin-top:3em"/>
    </div>
</div>

<div class="columns feature-shoutouts">
    <div class="column">
    	<h3>
            <span class="icon is-small"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="paperclip" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-paperclip fa-w-14"><path fill="currentColor" d="M43.246 466.142c-58.43-60.289-57.341-157.511 1.386-217.581L254.392 34c44.316-45.332 116.351-45.336 160.671 0 43.89 44.894 43.943 117.329 0 162.276L232.214 383.128c-29.855 30.537-78.633 30.111-107.982-.998-28.275-29.97-27.368-77.473 1.452-106.953l143.743-146.835c6.182-6.314 16.312-6.422 22.626-.241l22.861 22.379c6.315 6.182 6.422 16.312.241 22.626L171.427 319.927c-4.932 5.045-5.236 13.428-.648 18.292 4.372 4.634 11.245 4.711 15.688.165l182.849-186.851c19.613-20.062 19.613-52.725-.011-72.798-19.189-19.627-49.957-19.637-69.154 0L90.39 293.295c-34.763 35.56-35.299 93.12-1.191 128.313 34.01 35.093 88.985 35.137 123.058.286l172.06-175.999c6.177-6.319 16.307-6.433 22.626-.256l22.877 22.364c6.319 6.177 6.434 16.307.256 22.626l-172.06 175.998c-59.576 60.938-155.943 60.216-214.77-.485z" class=""></path></svg></span>
    		Attachments
    	</h3>
    	<p>Attach files to tasks, so you can always have all related files for a task in one place!</p>
    </div>
    <div class="column">
    	<h3>
	    	<span class="icon is-small"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="tasks" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-tasks fa-w-16"><path fill="currentColor" d="M139.61 35.5a12 12 0 0 0-17 0L58.93 98.81l-22.7-22.12a12 12 0 0 0-17 0L3.53 92.41a12 12 0 0 0 0 17l47.59 47.4a12.78 12.78 0 0 0 17.61 0l15.59-15.62L156.52 69a12.09 12.09 0 0 0 .09-17zm0 159.19a12 12 0 0 0-17 0l-63.68 63.72-22.7-22.1a12 12 0 0 0-17 0L3.53 252a12 12 0 0 0 0 17L51 316.5a12.77 12.77 0 0 0 17.6 0l15.7-15.69 72.2-72.22a12 12 0 0 0 .09-16.9zM64 368c-26.49 0-48.59 21.5-48.59 48S37.53 464 64 464a48 48 0 0 0 0-96zm432 16H208a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h288a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16zm0-320H208a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h288a16 16 0 0 0 16-16V80a16 16 0 0 0-16-16zm0 160H208a16 16 0 0 0-16 16v32a16 16 0 0 0 16 16h288a16 16 0 0 0 16-16v-32a16 16 0 0 0-16-16z" class=""></path></svg></span>
    		Relations
    	</h3>
    	<p>
    		Relate different tasks together - even if they're not on the same list! 
    		A relation can be multiple things, for example a subtask or blocking task.
    	</p>
    </div>
    <div class="column">
    	<h3>
    		<span class="icon is-small"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="calendar" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" class="svg-inline--fa fa-calendar fa-w-14"><path fill="currentColor" d="M12 192h424c6.6 0 12 5.4 12 12v260c0 26.5-21.5 48-48 48H48c-26.5 0-48-21.5-48-48V204c0-6.6 5.4-12 12-12zm436-44v-36c0-26.5-21.5-48-48-48h-48V12c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v52H160V12c0-6.6-5.4-12-12-12h-40c-6.6 0-12 5.4-12 12v52H48C21.5 64 0 85.5 0 112v36c0 6.6 5.4 12 12 12h424c6.6 0 12-5.4 12-12z" class=""></path></svg></span>
    		Due dates
    	</h3>
    	<p>
    		Remember these tasks with a deadline coming up? 
    		Set a due date for a task and see all tasks with upcoming deadlines at once!
    	</p>
    </div>
    <div class="column">
    	<h3>
    		<span class="icon is-small"><svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="sync" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-sync fa-w-16 fa-3x"><path fill="currentColor" d="M440.65 12.57l4 82.77A247.16 247.16 0 0 0 255.83 8C134.73 8 33.91 94.92 12.29 209.82A12 12 0 0 0 24.09 224h49.05a12 12 0 0 0 11.67-9.26 175.91 175.91 0 0 1 317-56.94l-101.46-4.86a12 12 0 0 0-12.57 12v47.41a12 12 0 0 0 12 12H500a12 12 0 0 0 12-12V12a12 12 0 0 0-12-12h-47.37a12 12 0 0 0-11.98 12.57zM255.83 432a175.61 175.61 0 0 1-146-77.8l101.8 4.87a12 12 0 0 0 12.57-12v-47.4a12 12 0 0 0-12-12H12a12 12 0 0 0-12 12V500a12 12 0 0 0 12 12h47.35a12 12 0 0 0 12-12.6l-4.15-82.57A247.17 247.17 0 0 0 255.83 504c121.11 0 221.93-86.92 243.55-201.82a12 12 0 0 0-11.8-14.18h-49.05a12 12 0 0 0-11.67 9.26A175.86 175.86 0 0 1 255.83 432z" class=""></path></svg></span>
    		CalDAV
    	</h3>
    	<p>Vikunja comes with a calDAV-integration which lets you use the tools you already use and love.</p>
    </div>
</div>

</div>
</div>

<section class="hero is-medium is-warning is-bold" style="margin: 4em 0 0">
  <div class="hero-body">
    <div class="container has-text-centered">
      <h1 class="title">
        Can't wait to see it in action?
      </h1>
      <a href="https://try.vikunja.io" class="button is-success is-large noshadow" rel="noopener">Try it</a>
    </div>
  </div>
</section>

<section class="hero is-medium is-primary is-bold" style="margin: 0 0 4em">
  <div class="hero-body">
    <div class="container has-text-centered">
    <div class="columns">
      <div class="column is-one-quarter has-text-right-desktop-only">
	    <img src="todoist.png" alt="Todoist"/>
	    <img src="trello.png" alt="Trello"/>
	    <img src="microsoft-todo.png" alt="Microsoft To-Do"/>
      </div>
      <div class="column">
        <h1 class="title">
          Import your data from Todoist, Trello or Microsoft To-Do
        </h1>
        <p>You can import your lists, tasks, notes and files from other providers into Vikunja in no time.</p>
      </div>
    </div>
  </div>
</section>

<div class="container is-default-color">
    <div class="content">
        <div class="columns">
            <div class="column is-half">

<h2>Self-Hosted</h2>

<p>Use Vikunja on your own server or hosted*. Your data will always be yours, we won't sell your grocery list to Amazon. Don't trust us? 
<a href="https://code.vikunja.io" rel="noopener">Check for yourself</a>.</p>

<p>
	Also, did we mention "the cloud"? 
	Vikunja syncs across all your devices, no matter if you created a task on the mobile app or on your laptop, 
	letting you continue where you left off.
<br/>
<small style="color:#636363">* Hosted instances of Vikunja will be available very soon!</small>

</p>

</div>
            <div class="column is-half">
<h2>Open-Source</h2>

<p>Vikunja is completly open source and released under the <a href="https://code.vikunja.io/api/src/branch/main/LICENSE" rel="noopener">GPLv3</a>.</p>

<p>You can contribute in multiple ways, even if you're not a developer! It already helps a lot when you use Vikunja and report 
<a href="https://code.vikunja.io/api/issues" rel="noopener">features you'd like or bugs you find</a>.</p>

<p>And in case you are a developer and know your way around <a href="https://code.vikunja.io/api" rel="noopener">Go</a> or 
<a href="https://code.vikunja.io/frontend" rel="noopener">Vue.js</a>, any help is appreciated.</p>

</div>
</div>

## Roadmap

But of course, there's more to come! Take a look at this list of things we've planned to add:

* Get all your tasks for an interval (day/month/period)
* Calendar view for tasks
* "Smart Lists" - Create lists based on filters
* Webhooks - Trigger other events when an action is done (like completing a task)
* Performance statistics - Get an overview and beautiful charts about what you got done this month
* Activity feeds - Get a quick overview about who did what
* Bulk-edit multiple tasks at once
* IMAP-Integration - Send an email to Vikunja to create a new task
* [Even more!](https://my.vikunja.cloud/share/QFyzYEmEYfSyQfTOmIRSwLUpkFjboaBqQCnaPmWd/auth)

<br/>

</div>
</div>