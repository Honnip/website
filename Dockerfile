FROM kolaente/toolbox AS build-theme

COPY . /web
WORKDIR /web
RUN mkdir /web/themes/vikunja -p && \
  cd /web/themes/vikunja && \
  wget https://dl.vikunja.io/theme/vikunja-theme.tar.gz && \
  tar -xzf vikunja-theme.tar.gz

FROM klakegg/hugo:0.99.1 as builder

COPY . /web
WORKDIR /web
COPY --from=build-theme /web/themes /web/themes

RUN rm -rf public && hugo

FROM nginx
COPY --from=builder /web/public /usr/share/nginx/html
ADD nginx.conf /etc/nginx/nginx.conf
