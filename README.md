# Website

[![Build Status](https://drone.kolaente.de/api/badges/vikunja/website/status.svg)](https://drone.kolaente.de/vikunja/website)

This repo contains all content and styling for [vikunja.io](https://vikunja.io).

## Sponsors

[![Relm](https://vikunja.io/images/sponsors/relm.png)](https://relm.us)


